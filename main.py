#!/usr/bin/env python3
from aiohttp import web
from aiohttp.web_response import Response
from time import time
import ujson as json
import logging
from systemd.journal import JournaldLogHandler

app = web.Application()

services = {}
log = logging.getLogger('keepalive')
log.addHandler(JournaldLogHandler())
log.setLevel(logging.INFO)

async def put_alive(request):
    log.info(request.url)
    service = request.match_info['service']
    node = request.match_info['node']
    try:
        services[service][node] = time()
    except KeyError:
        services[service] = {}
        services[service][node] = time()
    return Response(status=200, body=json.dumps(services[service]))

async def get_node(request):
    log.info(request.url)
    service = request.match_info['service']
    node = request.match_info['node']
    try:
        retdict = services[service][node]
    except KeyError:
        retdict = {}
    return Response(status=200, body=json.dumps(retdict))


async def get_service(request):
    log.info(request.url)
    service = request.match_info['service']
    svcdict = services[service] if service in services else {}
    return Response(status=200, body=json.dumps(svcdict))

async def get_allservices(request):
    log.info(request.url)
    return Response(status=200, body=json.dumps(services))

async def delete_service(request):
    log.info(request.url)
    service = request.match_info['service']
    if service in services:
        services.pop(service, None)
    return Response(status=200, body=json.dumps({}))

async def delete_node(request):
    log.info(request.url)
    service = request.match_info['service']
    node = request.match_info['node']
    try:
        if node in services[service]:
            services[service].pop(node, None)
    except KeyError:
        pass
    try:
        retdict = services[service]
    except KeyError:
        retdict = {}

    return Response(status=200, body=json.dumps(retdict))


app.add_routes([
    web.get('/service', get_allservices),
    web.get('/service/{service}', get_service),
    web.get('/service/{service}/{node}', get_node),
    web.put('/service/{service}/{node}', put_alive),
    web.delete('/service/{service}', delete_service),
    web.delete('/service/{service}/{node}', delete_node)
])

web.run_app(app, port=8080)
